# WebService RPC

Exemplo de webservice construído apenas com POST e com funções nomeadas por verbos; padrão inicial de uso de webservices que posteriormente deu lugar as webservice RESTful com foco no resource.

## Baixar o projeto...

Fazer o download do zip ou clonar com:

```
git clone https://gitlab.com/gilbriatore/2023/integra/ws-rpc.git
```

## Baixar o IntelliJ IDEA Community Edition...


- [ ] [IntelliJ IDEA Community Edition](https://www.jetbrains.com/idea/download)


Instalar a IDE, abrir o projeto e executar.
