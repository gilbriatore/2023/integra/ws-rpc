package briatore.rpc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Utiliza @Controller ao invés de @RestController */
@Controller
@RequestMapping("/ws-rpc")
public class RpcController {

    private final Map<Integer, Produto> produtos;

    RpcController() {
        this.produtos = new HashMap<>();
        this.produtos.put(1, new Produto(1, "Produto 1", 10.0));
        this.produtos.put(2, new Produto(2, "Produto 2", 20.0));
        this.produtos.put(3, new Produto(3, "Produto 3", 30.0));
        this.produtos.put(4, new Produto(4, "Produto 4", 40.0));
        this.produtos.put(5, new Produto(5, "Produto 5", 50.0));
    }

    /** Utiliza somente @PostMapping e expõe operações ao invés de recursos */
    @PostMapping("/listar-produtos")
    @ResponseBody
    public List<Produto> listarProdutos() {
        return produtos.values().stream().toList();
    }

    /** Utiliza somente @PostMapping e expõe operações ao invés de recursos */
    @PostMapping("/buscar-produto-por-codigo")
    @ResponseBody
    public Produto buscarProdutoPorCodigo(@RequestBody Map<String, String> params) {
        Integer codigo = Integer.parseInt(params.get("codigo"));
        return produtos.get(codigo);
    }

    /** Utiliza somente @PostMapping e expõe operações ao invés de recursos */
    @PostMapping("/salvar-produto")
    @ResponseBody
    public List<Produto> salvarProduto(@RequestBody Produto produto) {
        produto.setCodigo(getNovoCodigo());
        produtos.put(produto.getCodigo(), produto);
        return produtos.values().stream().toList();
    }

    /** Utiliza somente @PostMapping e expõe operações ao invés de recursos */
    @PostMapping("/atualizar-produto")
    @ResponseBody
    public List<Produto> atualizarProduto(@RequestBody Produto produto) {
        produtos.put(produto.getCodigo(), produto);
        return produtos.values().stream().toList();
    }

    /** Utiliza somente @PostMapping e expõe operações ao invés de recursos */
    @PostMapping("/excluir-produto")
    @ResponseBody
    public List<Produto> excluirProduto(@RequestBody Map<String, String> params) {
        Integer codigo = Integer.parseInt(params.get("codigo"));
        produtos.remove(codigo);
        return produtos.values().stream().toList();
    }

    private int getNovoCodigo() {
        int maiorCodigo = 0;
        for (Integer key : produtos.keySet()) {
            if (key > maiorCodigo) {
                maiorCodigo = key;
            }
        }
        return maiorCodigo + 1;
    }
}